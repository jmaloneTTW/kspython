import krpc
import math
import time
import threading
import os


conn = krpc.connect(name="KSPython")
vessel = conn.space_center.active_vessel
obt_frame = vessel.orbit.body.non_rotating_reference_frame
srf_frame = vessel.orbit.body.reference_frame


# Pre-launch setup
vessel.control.sas = False
vessel.control.rcs = False
vessel.control.throttle = 1.0

# Countdown...
# print('3...')
# time.sleep(1)
# print('2...')
# time.sleep(1)
# print('1...')
# time.sleep(1)
print('Launch!')

# Activate the first stage
vessel.control.activate_next_stage()
vessel.auto_pilot.engage()
vessel.auto_pilot.target_pitch_and_heading(90, 90)

obt_speed = 0
srf_speed = 0
altitude = 0
flight_data = {"Speed(Orbit)": obt_speed, "Speed(Surface)":srf_speed, "Altitude": altitude}

def collect_flight_data():
    #TODO remove duplicate variable calls
    global srf_speed
    global obt_speed
    global altitude
    global fuel_amount
    while True:
        obt_speed = vessel.flight(obt_frame).speed
        srf_speed = vessel.flight(srf_frame).speed
        altitude = conn.add_stream(getattr, vessel.flight(), 'mean_altitude')

        flight_data["Speed(Orbit)"] = obt_speed
        flight_data["Speed(Surface)"] = srf_speed
        flight_data["Altitude"] = altitude()

        time.sleep(1)

def deploy_chute():
    speed_chk = False
    while not speed_chk:
        print('Surface speed = %.1f m/s. Altitude = %2f.' % (flight_data["Speed(Surface)"], flight_data["Altitude"]))
        if vessel.resources.amount('SolidFuel') < 0.1:
            speed_chk = True
        time.sleep(1)

    while speed_chk:
        vessel.auto_pilot.target_pitch_and_heading(90, 90)
        print('Surface speed = %.1f m/s. Altitude = %2f.' % (flight_data["Speed(Surface)"], flight_data["Altitude"]))
        if flight_data["Speed(Surface)"] < 500 and flight_data["Altitude"] < 3000:
            speed_chk = False
            vessel.control.activate_next_stage()
            print("Chutes deployed!")
        time.sleep(1)



def goLaunch():
    deploy_chute()






get_flight_data = threading.Thread(target=collect_flight_data)
run_systems = threading.Thread(target=goLaunch)

get_flight_data.start()
run_systems.start()

get_flight_data.join()
run_systems.join()




# while True:
#     obt_speed = vessel.flight(obt_frame).speed
#     srf_speed = vessel.flight(srf_frame).speed
#     print('Orbital speed = %.1f m/s, Surface speed = %.1f m/s' %
#           (obt_speed, srf_speed))
#     time.sleep(1)

